import 'package:flutter/material.dart';
import 'package:pokedex/model/pokemon.dart';

class PowerLine extends StatelessWidget {
  final Stat stat;

  const PowerLine(this.stat, {super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      left: false,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Container(
          color: Colors.grey[300],
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 2.0),
            child: Stack(
              children: [
                _powerbar,
                _lineValue(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget get _powerbar => Positioned.fill(
        child: Align(
          alignment: Alignment.centerLeft,
          child: FractionallySizedBox(
            widthFactor: stat.statFactor,
            child: Container(color: Colors.red),
          ),
        ),
      );

  Widget _lineValue() => Padding(
        padding: const EdgeInsets.only(left: 5.0),
        child: Text(
          stat.baseStat.toString(),
          style: const TextStyle(color: Colors.white),
        ),
      );
}
