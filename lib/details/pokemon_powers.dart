import 'package:flutter/material.dart';
import 'package:pokedex/model/pokemon.dart';
import 'package:pokedex/details/power_line.dart';

class PokemonPowers extends StatelessWidget {
  final PokemonDetails pokemon;

  const PokemonPowers({super.key, required this.pokemon});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: pokemon.stats
                  .map(
                    (stat) => Padding(
                      padding: const EdgeInsets.all(7.0),
                      child: Text(stat.name!),
                    ),
                  )
                  .toList(),
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: pokemon.stats.map((stat) => PowerLine(stat)).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
