import 'package:flutter/material.dart';
import 'package:pokedex/common/utils.dart';
import 'package:pokedex/model/pokemon.dart';
import 'package:pokedex/details/types_row.dart';
import 'package:pokedex/common/pokemon_image.dart';
import 'package:pokedex/details/pokemon_powers.dart';
import 'package:pokedex/details/profile.dart';

class PokemonDetailsScreen extends StatelessWidget {
  final PokemonDetails pokemon;

  const PokemonDetailsScreen({super.key, required this.pokemon});

  Widget _buildSummary() {
    return Container(
      color: Colors.greenAccent,
      child: Row(
        children: [
          PokemonImage(pokemon: pokemon),
          PokemonPowers(pokemon: pokemon),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(pokemon.name.capitalize),
        backgroundColor: Colors.lightGreen,
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          TypesRow(pokemon),
          _buildSummary(),
          Profile(pokemon),
        ]),
      ),
    );
  }
}
