import 'package:flutter/material.dart';
import 'package:pokedex/model/pokemon.dart';

class TypesRow extends StatelessWidget {
  final PokemonDetails pokemon;

  const TypesRow(this.pokemon, {super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: pokemon.types
          .map(
            (type) => Expanded(
              child: Container(
                color: type.color,
                child: SafeArea(
                  top: false,
                  bottom: false,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(
                      type.name!,
                      style:
                          const TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                  ),
                ),
              ),
            ),
          )
          .toList(),
    );
  }
}
