import 'package:flutter/material.dart';
import 'package:pokedex/model/pokemon.dart';

class Profile extends StatelessWidget {
  final PokemonDetails pokemon;

  const Profile(this.pokemon, {super.key});

  Widget _header({required String text}) {
    return Container(
      height: 60.0,
      color: pokemon.types.first.darkColor,
      child: SafeArea(
        top: false,
        bottom: false,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              text,
              style: const TextStyle(
                fontSize: 22.0,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _profileRow(String title, String data) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.topLeft,
            width: 130.0,
            child: Text(
              title,
              style:
                  const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.topLeft,
              child: Text(
                data,
                style: const TextStyle(fontSize: 20.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _flavorText() {
    return SafeArea(
      top: false,
      bottom: false,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Text(
          pokemon.species.flavorText,
          style: const TextStyle(
            fontSize: 18.0,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _header(text: 'Profile'),
        SafeArea(
          top: false,
          bottom: false,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: [
                _profileRow('Weight:', '${pokemon.weight.toString()}kg'),
                _profileRow('Height:', '${pokemon.height.toString()}m'),
                _profileRow('Abilities:', pokemon.abilities.join(', ')),
                _profileRow(
                    'Egg groups:', pokemon.species.eggGroups.join(', ')),
              ],
            ),
          ),
        ),
        _header(text: pokemon.species.genus),
        _flavorText(),
      ],
    );
  }
}
