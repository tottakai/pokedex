import 'package:flutter/material.dart';
import 'package:pokedex/common/utils.dart';

class Pokemon {
  final String name;
  final String url;

  Pokemon({required this.name, required this.url});

  factory Pokemon.fromJson(dynamic json) {
    return Pokemon(name: json['name'] as String, url: json['url'] as String);
  }
}

class PokemonDetails {
  final int id;
  final String name;
  final String? imageUrl;
  final double height;
  final double weight;
  final List<Stat> stats;
  final List<Type> types;
  final List<String> abilities;
  final PokemonSpecies species;

  PokemonDetails(
      {required this.id,
      required this.name,
      required this.imageUrl,
      required this.height,
      required this.weight,
      required this.stats,
      required this.types,
      required this.abilities,
      required this.species});

  factory PokemonDetails.fromJson(dynamic json, PokemonSpecies species) {
    final List<Stat> stats =
        json['stats'].map<Stat>((stat) => Stat.fromJson(stat)).toList();
    final maxStat = stats.reduce((lhs, rhs) {
      return lhs.baseStat! > rhs.baseStat! ? lhs : rhs;
    }).baseStat;
    // ignore: avoid_function_literals_in_foreach_calls
    stats.forEach((stat) {
      stat.statMax = maxStat!.toDouble();
    });

    return PokemonDetails(
      id: json['id'],
      name: json['name'],
      imageUrl: json['sprites']['front_default'],
      height: json['height'] / 10.0,
      weight: json['weight'] / 10.0,
      stats: stats,
      types: json['types'].map<Type>((elem) => Type.fromJson(elem)).toList(),
      abilities: json['abilities']
          .map<String>((elem) => elem['ability']['name'].toString().capitalize)
          .toList(),
      species: species,
    );
  }
}

class Type {
  final Color color;
  final String? name;

  Type(this.color, this.name);

  Color get darkColor {
    return Color.fromRGBO((color.red * 0.8).round(),
        (color.green * 0.8).round(), (color.blue * 0.8).round(), 1.0);
  }

  factory Type.fromJson(Map<String, dynamic> json) {
    final String? name = json['type']['name'];
    var color = Colors.black;
    switch (name) {
      case 'grass':
        color = const Color.fromRGBO(105, 194, 61, 1.0);
        break;
      case 'poison':
        color = const Color.fromRGBO(146, 58, 146, 1.0);
        break;
      case 'fire':
        color = const Color.fromRGBO(237, 109, 18, 1.0);
        break;
      case 'flying':
        color = const Color.fromRGBO(142, 111, 235, 1.0);
        break;
      case 'water':
        color = const Color.fromRGBO(69, 120, 237, 1.0);
        break;
      case 'bug':
        color = const Color.fromRGBO(151, 165, 30, 1.0);
        break;
      case 'normal':
        color = const Color.fromRGBO(156, 156, 99, 1.0);
        break;
      case 'electric':
        color = const Color.fromRGBO(246, 201, 19, 1.0);
        break;
      case 'ground':
        color = const Color.fromRGBO(219, 181, 77, 1.0);
        break;
      case 'fairy':
        color = const Color.fromRGBO(232, 120, 144, 1.0);
        break;
      case 'fighting':
        color = const Color.fromRGBO(174, 43, 36, 1.0);
        break;
      case 'psychic':
        color = const Color.fromRGBO(247, 54, 112, 1.0);
        break;
      case 'rock':
        color = const Color.fromRGBO(164, 143, 50, 1.0);
        break;
      case 'steel':
        color = const Color.fromRGBO(160, 160, 192, 1.0);
        break;
      case 'ice':
        color = const Color.fromRGBO(126, 206, 206, 1.0);
        break;
      case 'ghost':
        color = const Color.fromRGBO(100, 78, 136, 1.0);
        break;
      case 'dragon':
        color = const Color.fromRGBO(94, 29, 247, 1.0);
        break;
      case 'dark':
        color = const Color.fromRGBO(100, 78, 64, 1.0);
        break;
    }
    return Type(color, name);
  }
}

class Stat {
  final String? name;
  final int? baseStat;
  double statMax = 100.0;

  Stat(this.name, this.baseStat);

  double get statFactor {
    return baseStat! / statMax;
  }

  factory Stat.fromJson(Map<String, dynamic> json) {
    return Stat(json['stat']['name'], json['base_stat']);
  }
}

class PokemonSpecies {
  final List<String> eggGroups;
  final String genus;
  final String flavorText;

  PokemonSpecies(this.eggGroups, this.genus, this.flavorText);

  factory PokemonSpecies.fromJson(
      Map<String, dynamic> json, String? versionName) {
    final String flavorTexts = (json['flavor_text_entries'] as List<dynamic>)
        .where((entry) =>
            entry['language']['name'].toString() == 'en' &&
            entry['version']['name'] == versionName)
        .map((entry) =>
            entry['flavor_text'].toString().replaceAll(RegExp('\\n|\\f'), ' '))
        .toList()
        .first;

    final genus = (json['genera'] as List<dynamic>)
        .where((entry) => entry['language']['name'] == 'en')
        .map((entry) => entry['genus'].toString())
        .toList()
        .first;

    final eggGroups = json['egg_groups']
        .map<String>((elem) => elem['name'].toString())
        .toList();
    return PokemonSpecies(eggGroups, genus, flavorTexts);
  }
}
