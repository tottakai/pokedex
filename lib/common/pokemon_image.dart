import 'package:flutter/material.dart';
import 'package:pokedex/model/pokemon.dart';

class PokemonImage extends StatefulWidget {
  final PokemonDetails pokemon;

  const PokemonImage({super.key, required this.pokemon});

  @override
  State<StatefulWidget> createState() => _PokemonImageState();
}

class _PokemonImageState extends State<PokemonImage>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);
    _controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: widget.pokemon.imageUrl != null
          ? ScaleTransition(
              scale: CurvedAnimation(
                  parent: _controller,
                  curve: const Interval(0.0, 1.0, curve: Curves.easeIn)),
              child: SizedBox(
                width: 140.0,
                child: Image.network(
                  widget.pokemon.imageUrl!,
                  scale: 0.6,
                  width: 140.0,
                  height: 140.0,
                ),
              ),
            )
          : const Icon(Icons.help),
    );
  }
}
