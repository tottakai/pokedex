import 'package:flutter/material.dart';
import 'package:pokedex/main_screen/main_screen.dart';

void main() => runApp(const PokedexApp());

class PokedexApp extends StatelessWidget {
  const PokedexApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Pokedex',
      home: MainScreen(),
    );
  }
}
