import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pokedex/model/pokemon.dart';

class Pokeapi {
  static Future<List<Pokemon>> getPokemonList() async {
    return http
        .get(Uri.parse('https://pokeapi.co/api/v2/pokemon?limit=1000'))
        .then((response) {
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);

        final filteredPokemons =
            List<dynamic>.from(responseJson['results']).where((elem) {
          final segments = Uri.parse(elem['url']).pathSegments;
          final id = int.tryParse(segments.reversed.skip(1).first) ?? 100000;
          return id < 10000;
        });

        return filteredPokemons
            .map<Pokemon>((json) => Pokemon.fromJson(json))
            .toList();
      }
      return [];
    });
  }

  static Future<PokemonDetails> getPokemon(String? url) async {
    var cacheHit = PokemonDetailsCache().get(url);
    if (cacheHit != null) {
      return Future.value(cacheHit);
    }

    return http.get(Uri.parse(url!)).then((response) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      final versionName = responseJson['game_indices'][0]['version']['name'];
      return http
          .get(Uri.parse(responseJson['species']['url']))
          .then((speciesResponse) {
        Map<String, dynamic> speciesResponseJson =
            json.decode(speciesResponse.body);
        PokemonSpecies species =
            PokemonSpecies.fromJson(speciesResponseJson, versionName);
        var details = PokemonDetails.fromJson(responseJson, species);
        PokemonDetailsCache().put(url, details);
        return details;
      });
    });
  }
}

class PokemonDetailsCache {
  static final _singleton = PokemonDetailsCache._internal();
  final Map<String?, PokemonDetails> _cache = {};

  PokemonDetails? get(String? url) {
    return _cache[url];
  }

  void put(String? url, PokemonDetails details) {
    _cache[url] = details;
  }

  factory PokemonDetailsCache() {
    return _singleton;
  }

  PokemonDetailsCache._internal();
}
