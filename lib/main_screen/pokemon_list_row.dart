import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/network/pokeapi.dart';
import 'package:pokedex/model/pokemon.dart';
import 'package:pokedex/details/details.dart';
import 'package:pokedex/main_screen/pokemon_tile.dart';

class PokemonListRow extends StatefulWidget {
  final Pokemon pokemon;

  const PokemonListRow({super.key, required this.pokemon});

  @override
  State<StatefulWidget> createState() => _PokemonListRowState();
}

class _PokemonListRowState extends State<PokemonListRow> {
  PokemonDetails? pokemonDetails;
  CancelableOperation<PokemonDetails>? getPokemon;

  @override
  void dispose() {
    if (getPokemon != null) {
      getPokemon!.cancel();
    }
    super.dispose();
  }

  Future<void> _loadPokemon() {
    getPokemon =
        CancelableOperation.fromFuture(Pokeapi.getPokemon(widget.pokemon.url));

    return getPokemon!.value.then((response) {
      if (mounted) {
        setState(() {
          pokemonDetails = response;
        });
      }
    });
  }

  @override
  void didUpdateWidget(PokemonListRow oldWidget) {
    super.didUpdateWidget(oldWidget);
    _loadPokemon();
  }

  @override
  void initState() {
    super.initState();
    _loadPokemon();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _openPokemonDetailsScreen(pokemonDetails),
      child: GridTile(
        child: pokemonDetails != null
            ? PokemonTile(pokemon: pokemonDetails!)
            : Container(),
      ),
    );
  }

  Future<void> _openPokemonDetailsScreen(PokemonDetails? pokemonDetails) async {
    if (pokemonDetails == null) {
      return;
    }
    return Navigator.push<void>(
      context,
      MaterialPageRoute(
        builder: (context) => PokemonDetailsScreen(pokemon: pokemonDetails),
      ),
    );
  }
}
