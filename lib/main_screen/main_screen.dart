import 'package:flutter/material.dart';
import 'package:pokedex/network/pokeapi.dart';
import 'package:pokedex/model/pokemon.dart';
import 'package:pokedex/main_screen/pokemon_list_row.dart';

class MainScreen extends StatefulWidget {
  final String title;

  const MainScreen({super.key}) : title = 'Pokedex';

  @override
  MainScreenState createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  final TextEditingController _filter = TextEditingController();
  List<Pokemon> pokemons = [];
  List<Pokemon> filteredPokemons = [];
  Icon actionIcon = const Icon(Icons.search);
  Widget? appBarTitle;

  MainScreenState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          filteredPokemons = pokemons;
        });
      } else {
        setState(() {
          filteredPokemons = pokemons
              .where((pokemon) => pokemon.name
                  .toLowerCase()
                  .startsWith(_filter.text.toLowerCase()))
              .toList();
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    appBarTitle = Text(widget.title);

    Pokeapi.getPokemonList().then((res) {
      setState(() {
        pokemons = res;
        filteredPokemons = res;
      });
    });
  }

  Widget _buildPokemonList() {
    return GridView.builder(
      itemCount: filteredPokemons.length,
      itemBuilder: (context, index) {
        final pokemon = filteredPokemons[index];
        return PokemonListRow(
          pokemon: pokemon,
          key: Key('P_$index'),
        );
      },
      gridDelegate:
          const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
    );
  }

  void _searchPressed() {
    setState(() {
      if (actionIcon.icon == Icons.search) {
        actionIcon = const Icon(Icons.close);
        appBarTitle = TextField(
          controller: _filter,
          style: const TextStyle(
            color: Colors.white,
          ),
          decoration: const InputDecoration(
            prefixIcon: Icon(Icons.search, color: Colors.white),
            hintText: 'Search...',
            hintStyle: TextStyle(color: Colors.white),
          ),
        );
      } else {
        actionIcon = const Icon(Icons.search);
        appBarTitle = Text(widget.title);
        _filter.clear();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: actionIcon,
          onPressed: _searchPressed,
        ),
        title: appBarTitle,
      ),
      body: _buildPokemonList(),
    );
  }
}
