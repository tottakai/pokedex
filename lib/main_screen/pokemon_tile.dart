import 'package:flutter/material.dart';
import 'package:pokedex/model/pokemon.dart';
import 'package:pokedex/common/pokemon_image.dart';

class PokemonTile extends StatelessWidget {
  final PokemonDetails pokemon;

  const PokemonTile({super.key, required this.pokemon});

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        _buildBackgroundColorPattern(),
        PokemonImage(pokemon: pokemon),
      ],
    );
  }

  Widget _buildBackgroundColorPattern() {
    return Row(
      children: pokemon.types
          .map((type) => Expanded(child: Container(color: type.color)))
          .toList(),
    );
  }
}
